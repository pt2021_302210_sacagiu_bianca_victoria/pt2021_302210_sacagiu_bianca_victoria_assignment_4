package BusinessLayer;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Date: 16.05.2021
 */
public interface IDeliveryServiceProcessing {
    void importCSV();

    /**
     * @pre m != null
     * @post product != null
     * @param m product to add
     * @return add an item
     */
    void addProduct(MenuItem m)
    ;
    /**
     * @pre m != null
     * @param m product to delete
     * @return delete an item
     */
    void deleteProduct(MenuItem m);

    /**
     * @pre m1 != null
     * @pre m2 !=null
     * @post products != null
     * @param m1 edited item
     * @param m2 item to edit
     * @return edit an item
     */
    void editProduct(MenuItem m1, MenuItem m2);

    HashSet<Order> getOrders();

    Order addOrder(ArrayList<MenuItem> menus, int clientID);

    HashSet<MenuItem> getMenu();

    /**
     * @pre title != null
     * @post product != null
     * @param title the title of the product
     * @return edit an item
     */
    MenuItem findByName(String title);

    /**
     * @pre s1 != ""
     * @pre s2 >= 0
     * @pre s3 >= 0
     * @pre s4 >= 0
     * @pre s5 >= 0
     * @pre s6 >= 0
     * @pre s7 >= 0
     * @post list != null
     * @param s1 Title
     * @param s2 Rating
     * @param s3 Calories
     * @param s4 Protein
     * @param s5 Fat
     * @param s6 Sodium
     * @param s7 Price
     * @return List<MenuItem>
     */
    HashSet<MenuItem> search(String s1, String s2, String s3, String s4, String s5, String s6, String s7);

    void notifyClient(Order order);

    /**
     * @pre start must be >= 0
     * @pre end must be >= 0
     * @param start start hour
     * @param end end hour
     * @return generate a file
     */
    void rep1(int start, int end);

    /**
     * @pre ordNr must be >= 0
     * @param ordNr order number
     * @return generate a file
     */
    void rep2(int ordNr);

    /**
     * @pre ordNr must be >= 0
     * @pre maxValue must be >= 0
     * @param ordNr order number
     * @param maxValue max value for orders
     * @return generate a file
     */
    void rep3(int ordNr, int maxValue);

    /**
     * @pre day must be >= 0
     * @param day order day
     * @return generate a file
     */
    void rep4(int day);





}
