package BusinessLayer;

import java.util.Objects;

public class BaseProduct extends MenuItem{
    private float rating;
    private int calories;
    private int protein;
    private int fat;
    private int sodium;
    private int price;
    public int orderNr = 0;

    public BaseProduct(){
        super();
    }
    public BaseProduct(String title,float rating,int calories,int protein,int fat,int sodium,int price) {
        super(title);
        this.rating = rating;
        this.calories = calories;
        this.protein = protein;
        this.fat = fat;
        this.sodium = sodium;
        this.price = price;
    }

    @Override
    public int getPrice() {
        return this.price;
    }
    public float getRating()
    {
        return this.rating;
    }
    public int getCalories()
    {
        return this.calories;
    }
    public int getProtein()
    {
        return this.protein;
    }
    public int getFat()
    {
        return this.fat;
    }
    public int getSodium()
    {
        return this.sodium;
    }
    public void setRating(float param)
    {
        this.rating=param;
    }
    public void setCalories(int param)
    {
        this.calories=param;
    }
    public void setProtein(int param)
    {
        this.protein=param;
    }
    public void setFat(int param)
    {
        this.fat=param;
    }
    public void setSodium(int param)
    {
        this.sodium=param;
    }
    public void setPrice(int param)
    {
        this.price=param;
    }
    @Override
    public boolean equals(Object o) {
        if (((BaseProduct) o).getTitle().equals(this.getTitle()) == true)
            return true;

        return false;
    }
    @Override
    public int hashCode(){
        return Objects.hash(getTitle());
    }

    @Override
    public int getOrderNr() {
        return orderNr;
    }

    public void orderNrplus(){
        this.orderNr ++;
    }
}
