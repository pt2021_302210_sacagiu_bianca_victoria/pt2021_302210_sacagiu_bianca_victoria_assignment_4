package BusinessLayer;

import java.io.Serializable;

public class User implements Serializable {
    private String username;
    private String password;
    private Role type;
    private static int k = 1;
    private int ID = 0;
    private int comenzi;

    public User(String username, String password, Role role){
        this.username = username;
        this.password = password;
        this.type = role;
        if(this.type == Role.Admin){
            this.ID = 1;
        }
        else {
            k++;
            this.ID = k;
        }
        this.comenzi = 0;
    }

    public int getID() {
        return ID;
    }

    public Role getType() {
        return type;
    }

    public String getPass() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static int getK(){
        return k;
    }

    public static void setK(int c){
        k = c;
    }

    public void incComenzi(){
        this.comenzi++;
    }

    public int getComenzi() {
        return comenzi;
    }
}
