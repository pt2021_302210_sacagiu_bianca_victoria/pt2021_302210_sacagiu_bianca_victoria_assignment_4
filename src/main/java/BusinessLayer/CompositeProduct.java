package BusinessLayer;

import java.util.ArrayList;
import java.util.Objects;

public class CompositeProduct extends MenuItem{
    private ArrayList<BaseProduct> products = new ArrayList<>();
    private int orderNr = 0;

    public CompositeProduct() {};

    public void addProduct(BaseProduct product) {
        products.add(product);
    }

    public int getPrice() {
        int price = 0;
        for(BaseProduct i : products) {
            price += i.getPrice();
        }
        return price;
    }
    public float getRating() {
        float rating = 0f;
        for(BaseProduct i : products) {
            rating += i.getRating();
        }
        rating /= products.size();
        return rating;
    }
    public int getCalories() {
        int calories = 0;
        for(BaseProduct i : products) {
            calories += i.getCalories();
        }
        return calories;
    }
    public int getProtein() {
        int protein = 0;
        for(BaseProduct i : products) {
            protein += i.getProtein();
        }
        return protein;
    }
    public int getFat() {
        int fat = 0;
        for(BaseProduct i : products) {
            fat += i.getFat();
        }
        return fat;
    }
    public int getSodium() {
        int sodium = 0;
        for(BaseProduct i : products) {
            sodium += i.getSodium();
        }
        return sodium;
    }
    @Override
    public boolean equals(Object o) {
        BaseProduct other = (BaseProduct) o;
        if (((BaseProduct) o).getTitle().equals(this.getTitle()) == true)
            return true;

        return false;
    }
    @Override
    public int hashCode(){
        return Objects.hash(getTitle());
    }

    public void orderNrplus(){
        this.orderNr ++;
    }

    @Override
    public int getOrderNr() {
        return orderNr;
    }
}
