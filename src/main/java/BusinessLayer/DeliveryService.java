package BusinessLayer;

import DataLayer.CreateBill;

import java.io.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Date: 15.05, 2021
 */
public class DeliveryService extends Observable implements IDeliveryServiceProcessing {
    private static HashSet<MenuItem> menu = new HashSet<>();
    private CreateBill createBill = new CreateBill();
    private static HashSet<Order> orders = new HashSet<>();

    public static void setOrders(HashSet<Order> orders) {
        DeliveryService.orders = orders;
    }

    public static void setMenu(HashSet<MenuItem> menu) {
        DeliveryService.menu = menu;
    }

    public void importCSV() {
        ArrayList<MenuItem> menuItemArrayList = new ArrayList<>();
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\An 2\\Sem2\\TP\\tema4\\src\\main\\resources\\products.csv"))) {
            String line = null;
            br.readLine();
            while (true) {
                try {
                    if (!((line = br.readLine()) != null)) break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String[] values = line.split(",");
                records.add(Arrays.asList(values));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < records.size(); i ++){
                menuItemArrayList.add(new BaseProduct(records.get(i).get(0), Float.parseFloat(records.get(i).get(1)), Integer.parseInt(records.get(i).get(2)),
                        Integer.parseInt(records.get(i).get(3)), Integer.parseInt(records.get(i).get(4)), Integer.parseInt(records.get(i).get(5)),
                        Integer.parseInt(records.get(i).get(6))));
        }
        menu.addAll(menuItemArrayList);
    }

    public void addProduct(MenuItem menuItem) {
        assert menuItem != null : "menuItem != null";
        menu.add(menuItem);
        assert menu != null : "menu != null";
    }

    public HashSet<Order> getOrders() {
        return orders;
    }

    public void editProduct(MenuItem newMenuItem, MenuItem oldMenuItem) {
        for (MenuItem m : menu) {
            if (m.getTitle().equals(oldMenuItem.getTitle())) {
                menu.remove(m);
                menu.add(newMenuItem);
            }
        }
    }

    public void deleteProduct(MenuItem menuItem) {
        menu.remove(menuItem);
    }

    public Order addOrder(ArrayList<MenuItem> menus, int clientID) {
        Order order = new Order(menus, clientID);
        createBill(order, clientID);
        notifyClient(order);
        orders.add(order);
        return order;
    }

    public HashSet<MenuItem> getMenu() {
        return menu;
    }

    public HashSet<MenuItem> productsView() {
        return menu;
    }


    public static String[] getTitles() {
        int size = menu.size();
        String[] ans = new String[size];
        int j = 0;
        for(MenuItem m: menu){
            String title = m.getTitle();
            ans[j] = title;
            j++;
        }
        return ans;
    }

    public MenuItem findByName(String title) {
        for (MenuItem m : menu) {
            if (m.getTitle().equals(title)) {
                return m;
            }
        }
        return null;
    }

    public HashSet<MenuItem> search(String s1, String s2, String s3, String s4, String s5, String s6, String s7) {
        HashSet<MenuItem> aux = new HashSet<>();
        List<MenuItem> stream = menu.stream()
                .filter(menuItem1 -> menuItem1.getTitle().toLowerCase().contains(s1.toLowerCase()))
                .filter(menuItem1 -> Float.valueOf(s2) == menuItem1.getRating())
                .filter(menuItem1 -> Integer.valueOf(s3) == menuItem1.getCalories())
                .filter(menuItem1 -> Integer.valueOf(s4) == menuItem1.getProtein())
                .filter(menuItem1 -> Integer.valueOf(s5) == menuItem1.getFat())
                .filter(menuItem1 -> Integer.valueOf(s6) == menuItem1.getSodium())
                .filter(menuItem1 -> Integer.valueOf(s7) == menuItem1.getPrice())
                .collect(Collectors.toList());
        aux.addAll(stream);
        return aux;
    }

    public void notifyClient(Order order) {
        String str = "";
        str = str + order.getID() + ", " + order.getClientID() + ", ";
        for (MenuItem m : order.getOrder()) {
            str = str + m.getTitle() + ", ";
        }
        str = str + order.getPrice();
        setChanged();
        notifyObservers(str);
    }

    public void rep1(int start, int end) {
        assert start >= 0 : "Start cant be < 0 !";
        assert end >= 0 : "End cant be < 0 !";
        List<Order> stream = orders.stream()
                .filter(order -> order.getDate().getHours() >= start && order.getDate().getHours() <= end)
                .collect(Collectors.toList());
        createBill.report1((ArrayList<Order>) stream);
    }

    public void rep2(int ordNr) {
        //ArrayList<MenuItem> ord = new ArrayList<>();
        assert ordNr >= 0 : "ordNr cant be < 0 !";
        List<MenuItem> stream = menu.stream()
                .filter(menuItem -> menuItem.getOrderNr() >= ordNr)
                .collect(Collectors.toList());
        createBill.report2((ArrayList<MenuItem>) stream);
    }

    public void rep3(int ordNr, int maxValue) {
        assert ordNr >= 0 : "ordNr cant be < 0 !";
        assert maxValue >= 0 : "maxValue cant be < 0 !";
        List<Order> stream = orders.stream()
                .filter(order -> order.getPrice() >= maxValue)
                .collect(Collectors.toList());
        for (Order o : stream) {
            for (User u : Model.getUsers()) {
                if (u.getID() == o.getClientID()) {
                    u.incComenzi();
                }
            }
        }
        List<User> stream1 = Model.getUsers().stream()
                .filter(user -> user.getComenzi() >= ordNr)
                .collect(Collectors.toList());
        createBill.report3((ArrayList<User>) stream1);
    }

    public void rep4(int day) {
        ArrayList<MenuItem> m = new ArrayList<>();
        assert day >= 0 : "day cant be < 0 !";
        List<Order> stream = orders.stream()
                .filter(order -> order.getDate().getDay() == day)
                .collect(Collectors.toList());
        for (Order o : stream) {
            m.addAll(o.getOrder());
        }
        createBill.report4(m);
    }

    public boolean wellForm() {
        for (User us : Model.getUsers()) {
            if (us.getType() == Role.Employee)
                return true;
        }
        return false;
    }

    public void createBill(Order order, int clientID) {
        String filename = "bill.txt";
        new File(filename);
        FileWriter fname = null;
        try {
            fname = new FileWriter(filename);
            fname.write("BILL" + "\n\n");
            fname.write("Products: " + "\n");
            ArrayList<MenuItem> menuItems = order.getOrder();
            for (MenuItem m : menuItems) {
                fname.write(m.getTitle() + ", ");
            }
            fname.write("\nPrice: " + order.getPrice() + "\n\n");
            fname.write("Order ID: " + order.getID() + "\n");
            fname.write("Client ID: " + " " + order.getClientID() + "\n\n");
            fname.write("Date: " + order.getDate());

            fname.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
