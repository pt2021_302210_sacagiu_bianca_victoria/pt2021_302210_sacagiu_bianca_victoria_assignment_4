package BusinessLayer;

import java.util.ArrayList;

public class Model {

    private static ArrayList<User> users = new ArrayList<>();

    public void addUser(User u){
        this.users.add(u);
    }

    public static ArrayList<User> getUsers() {
        return users;
    }

    public static void setUsers(ArrayList<User> users) {
        Model.users = users;
    }

    public static int getUserID(String username, String password){
        for(User u: users){
            if(u.getUsername().equals(username) == true){
                    return u.getID();
            }
        }
        return -1;
    }

}
