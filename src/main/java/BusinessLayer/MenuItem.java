package BusinessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {
    private String title;
    public MenuItem(){};
    public MenuItem(String title){
        this.title = title;
    }
    public abstract int getPrice();
    public String getTitle(){
        return this.title;
    }
    public void setTitle(String t){
        this.title = t;
    }
    public abstract float getRating();
    public abstract int getCalories();
    public abstract int getProtein();
    public abstract int getFat();
    public abstract int getSodium();
    public abstract boolean equals(Object o);
    public abstract int hashCode();
    public abstract int getOrderNr();
    public abstract void orderNrplus();

}
