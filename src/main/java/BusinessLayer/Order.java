package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Order implements Serializable {
    private ArrayList<MenuItem> order = new ArrayList<>();
    private static int contor = 0;
    private int ID = 0;
    private int clientID;
    private Date date;

    public Order(ArrayList<MenuItem> menu, int clientID){
        contor++;
        this.ID = contor;
        order.addAll(menu);
        this.clientID = clientID;
        date = new Date();
    }

    public int getClientID() {
        return clientID;
    }

    public Date getDate() {
        return date;
    }

    public int getID() {
        return ID;
    }

    public ArrayList<MenuItem> getOrder() {
        return this.order;
    }

    public void setOrder(ArrayList<MenuItem> order) {
        this.order = order;
    }

    public float getPrice(){
        float sum = 0;
        for(MenuItem m: order){
            sum += m.getPrice();
        }
        return sum;
    }

    public static int getContor(){
        return contor;
    }

    public static void setContor(int c){
        contor = c;
    }
}
