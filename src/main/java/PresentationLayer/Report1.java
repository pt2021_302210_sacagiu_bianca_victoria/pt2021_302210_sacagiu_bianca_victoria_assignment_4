package PresentationLayer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class Report1 extends JFrame{
    JButton btnBack;
    JButton importBtn;
    private JPanel contentPane;
    JTextField startTxt;
    JTextField endTxt;
    JButton makeReport1Btn;

    public Report1(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 474, 409);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(204, 255, 153));
        contentPane.setForeground(new Color(0, 153, 0));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel adminLbl = new JLabel("Report 1");
        adminLbl.setForeground(new Color(0, 102, 0));
        adminLbl.setFont(new Font("Times New Roman", Font.BOLD, 38));
        adminLbl.setBounds(151, 10, 368, 50);
        contentPane.add(adminLbl);

        btnBack = new JButton("Back");
        btnBack.setForeground(new Color(0, 102, 0));
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 20));
        btnBack.setBackground(new Color(255, 255, 51));
        btnBack.setBounds(10, 315, 85, 35);
        contentPane.add(btnBack);

        makeReport1Btn = new JButton("Make report");
        makeReport1Btn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        makeReport1Btn.setForeground(new Color(0, 102, 0));
        makeReport1Btn.setBackground(new Color(255, 255, 51));
        makeReport1Btn.setBounds(125, 266, 202, 35);
        contentPane.add(makeReport1Btn);

        JLabel lblNewLabel = new JLabel("Start hour:");
        lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 26));
        lblNewLabel.setForeground(new Color(0, 102, 0));
        lblNewLabel.setBounds(37, 103, 177, 35);
        contentPane.add(lblNewLabel);

        startTxt = new JTextField();
        startTxt.setFont(new Font("Times New Roman", Font.BOLD, 23));
        startTxt.setForeground(new Color(0, 102, 0));
        startTxt.setBackground(new Color(255, 255, 51));
        startTxt.setBounds(185, 106, 158, 32);
        contentPane.add(startTxt);
        startTxt.setColumns(10);

        JLabel lblNewLabel_1 = new JLabel("End hour:");
        lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 26));
        lblNewLabel_1.setForeground(new Color(0, 102, 0));
        lblNewLabel_1.setBounds(37, 163, 138, 32);
        contentPane.add(lblNewLabel_1);

        endTxt = new JTextField();
        endTxt.setForeground(new Color(0, 102, 0));
        endTxt.setFont(new Font("Times New Roman", Font.BOLD, 23));
        endTxt.setBackground(new Color(255, 255, 51));
        endTxt.setBounds(185, 168, 158, 32);
        contentPane.add(endTxt);
        endTxt.setColumns(10);
    }

    public JButton getBtnBack() {
        return btnBack;
    }


    public JButton getMakeReport1Btn() {
        return makeReport1Btn;
    }

    public String getEndTxt() {
        return endTxt.getText();
    }

    public String getStartTxt() {
        return startTxt.getText();
    }
}
