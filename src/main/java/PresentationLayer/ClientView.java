package PresentationLayer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class ClientView extends JFrame{
    private JButton orderBtn;
    private JButton btnBack;
    private JButton showProductsBtn;
    private JButton searchBtn;
    private JPanel contentPane;

    public ClientView(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 474, 388);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(153, 204, 204));
        contentPane.setForeground(new Color(0, 153, 0));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel adminLbl = new JLabel("CLIENT");
        adminLbl.setForeground(new Color(102, 102, 153));
        adminLbl.setFont(new Font("Times New Roman", Font.BOLD, 38));
        adminLbl.setBounds(138, 10, 368, 50);
        contentPane.add(adminLbl);

        btnBack = new JButton("Back");
        btnBack.setForeground(new Color(102, 102, 153));
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 20));
        //btnBack.setBackground(new Color(255, 255, 204));
        btnBack.setBounds(10, 300, 85, 35);
        contentPane.add(btnBack);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setBounds(282, 299, 178, -118);
        contentPane.add(tabbedPane);

        showProductsBtn = new JButton("Show products");
       // showProductsBtn.setBackground(new Color(255, 255, 204));
        showProductsBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        showProductsBtn.setForeground(new Color(102, 102, 153));
        showProductsBtn.setBounds(32, 98, 166, 43);
        contentPane.add(showProductsBtn);

        searchBtn = new JButton("Search Product");
        searchBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        searchBtn.setForeground(new Color(102, 102, 153));
        searchBtn.setBounds(119, 173, 166, 43);
        contentPane.add(searchBtn);

        orderBtn = new JButton("Order create");
        orderBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        orderBtn.setForeground(new Color(102, 102, 153));
        //orderBtn.setBackground(new Color(255, 255, 204));
        orderBtn.setBounds(229, 246, 166, 43);
        contentPane.add(orderBtn);
    }

    public JButton getBtnBack() {
        return btnBack;
    }

    public JButton getShowProductsBtn() {
        return showProductsBtn;
    }

    public JButton getSearchBtn() {
        return searchBtn;
    }

    public JButton getOrderBtn() {
        return orderBtn;
    }
}