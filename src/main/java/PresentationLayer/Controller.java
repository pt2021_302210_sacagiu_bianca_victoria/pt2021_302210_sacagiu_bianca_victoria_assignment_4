package PresentationLayer;
import BusinessLayer.*;
import DataLayer.CreateBill;
import DataLayer.Serializator;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;

public class Controller {
    private View view = new View();
    CreateBill createBill;
    Report3 report3;
    private ViewAdd viewAdd;
    private Model model;
    private AdministratorView administratorView;
    private ClientView clientView;
    private EmployeeView employeeView;
    private Report2 report2;
    private Report4 report4;
    private JTable table;
    private DeliveryService deliveryService = new DeliveryService();
    private ProductsView productsView;
    private AddProductView addProductView;
    private EditProductView editProductView;
    private DeleteProductView deleteProductView;
    private CompositeProductView compositeProductView;
    private OrderView orderView;
    public Report1 report1;
    public HashSet<Order> orderHashSet = new HashSet<>();
    private int clientID;
    JTable tab;
    private SearchView searchView;
    int variable = 0;
    public Controller(Model model){
        Serializator.deser();
        employeeView = new EmployeeView();
        deliveryService.addObserver(employeeView);
        assert deliveryService.wellForm() == true;
        model.addUser(new User("Andreea", "123", Role.Employee));
        employeeView.setVisible(false);
        int k = 0;
        for(User u: Model.getUsers()) {
            if (u.getType() == Role.Admin)
                k = 1;
        }
        if(k == 0) {
            model.addUser(new User("Bianca", "parola", Role.Admin));
        }
        this.model = model;
        view.setVisible(true);
        view.getBtnNew().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewAdd = new ViewAdd();
                view.setVisible(false);
                viewAdd.setVisible(true);
                viewAdd.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewAdd.dispose();
                        view.setVisible(true);
                    }
                });
                viewAdd.getBtnIn().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int ok = 1;
                        for(int i = 0; i < model.getUsers().size(); i ++){
                            if(viewAdd.getNameTxt().equals(model.getUsers().get(i).getUsername())){
                                JOptionPane.showMessageDialog(null,
                                        "The username is already taken.",
                                        "WARNING",
                                        JOptionPane.WARNING_MESSAGE);
                                ok = 0;
                            }
                        }
                        if(ok == 1)
                            model.addUser(new User(viewAdd.getNameTxt(), viewAdd.getPassTxt(), viewAdd.getComboBox()));
                            viewAdd.clear();
                    }
                });
            }
        });
        view.getLoginButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ok = 0;
                String username = view.getUsername();
                String password = view.getPassword();
                clientID = Model.getUserID(username, password);
                for(int i = 0; i < model.getUsers().size(); i ++){
                    System.out.println(model.getUsers().get(i).getUsername());
                }
                for(int i = 0; i < model.getUsers().size(); i ++) {
                    if(view.getNameTxt().equals(model.getUsers().get(i).getUsername())){
                        if(view.getPass().equals(model.getUsers().get(i).getPass())) {
                            ok = 1;
                        }
                    }
                }
                if(ok == 0){
                    JOptionPane.showMessageDialog(null,
                            "Something is wrong!",
                            "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                }
                if(ok == 1){
                    if(view.getNameTxt().equals("Bianca")){
                        administratorView = new AdministratorView();
                        administratorView.setVisible(true);
                        view.setVisible(false);
                        ///BACK
                        administratorView.getBtnBack().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                administratorView.dispose();
                                view.setVisible(true);
                            }
                        });

                        ///IMPORT DATA
                        administratorView.importBtn.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                               deliveryService.importCSV();
                               System.out.println(deliveryService.getMenu().size());
                            }
                        });
                        ////ADD PRODUCT
                        administratorView.addBtn.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                addProductView = new AddProductView();
                                addProductView.setVisible(true);
                                administratorView.setVisible(false);
                                addProductView.getBtnBack().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        addProductView.setVisible(false);
                                        administratorView.setVisible(true);
                                    }
                                });
                                addProductView.getBtnNewButton().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        deliveryService.addProduct(new BaseProduct(addProductView.getField1(),
                                                Float.valueOf(addProductView.getField2()), Integer.valueOf(addProductView.getField3()),
                                                Integer.valueOf(addProductView.getField4()), Integer.valueOf(addProductView.getField5()),
                                                Integer.valueOf(addProductView.getField6()), Integer.valueOf(addProductView.getField7())));
                                        addProductView.clear();
                                    }
                                });
                            }
                        });
                        //////EDIT PRODUCT
                        administratorView.editBtn.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                editProductView = new EditProductView();
                                editProductView.setVisible(true);
                                administratorView.setVisible(false);
                                editProductView.getBtnBack().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        editProductView.dispose();
                                        administratorView.setVisible(true);
                                    }
                                });
                                editProductView.getBtnEdit().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {

                                        deliveryService.editProduct(new BaseProduct(editProductView.getField1(),
                                                Float.valueOf(editProductView.getField2()), Integer.valueOf(editProductView.getField3()),
                                                Integer.valueOf(editProductView.getField4()), Integer.valueOf(editProductView.getField5()),
                                                Integer.valueOf(editProductView.getField6()), Integer.valueOf(editProductView.getField7())),
                                                deliveryService.findByName(editProductView.getCombo()));
                                    }
                                });
                            }
                        });
                        //////DELETE
                        administratorView.deleteBtn.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                deleteProductView = new DeleteProductView();
                                deleteProductView.setVisible(true);
                                administratorView.setVisible(false);
                                deleteProductView.getBackBtn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        deleteProductView.dispose();
                                        administratorView.setVisible(true);
                                    }
                                });
                                deleteProductView.getDeleteBtn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        deliveryService.deleteProduct(deliveryService.findByName(deleteProductView.getCombo()));
                                    }
                                });
                            }
                        });
                        /////COMPOSITE PRODUCT
                        administratorView.addComposedBtn.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                compositeProductView = new CompositeProductView();
                                compositeProductView.setVisible(true);
                                administratorView.setVisible(false);
                                compositeProductView.getBackBtn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        compositeProductView.dispose();
                                        administratorView.setVisible(true);
                                    }
                                });
                                CompositeProduct a = new CompositeProduct();
                                compositeProductView.getPutBtn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {

                                        if(deliveryService.findByName((compositeProductView.getCombo())) instanceof MenuItem) {
                                            a.addProduct((BaseProduct) deliveryService.findByName(compositeProductView.getCombo()));
                                            variable = 1;
                                        }
                                        else
                                            JOptionPane.showMessageDialog(null,
                                                    "Something is wrong!",
                                                    "ERROR",
                                                    JOptionPane.INFORMATION_MESSAGE);
                                    }
                                });
                                compositeProductView.getAddBtn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        if(variable == 1) {
                                            a.setTitle(compositeProductView.getTitle());
                                            deliveryService.addProduct(a);
                                        }
                                    }
                                });
                            }
                        });

                        //////REPORT 1
                        administratorView.getReport1Btn().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                report1 = new Report1();
                                report1.setVisible(true);
                                administratorView.setVisible(false);
                                report1.getBtnBack().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        report1.dispose();
                                        administratorView.setVisible(true);
                                    }
                                });
                                report1.getMakeReport1Btn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        deliveryService.rep1(Integer.valueOf(report1.getStartTxt()),
                                                Integer.valueOf(report1.getEndTxt()));
                                    }
                                });
                            }
                        });

                        /////REPORT 2
                        administratorView.getReport2Btn().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                report2 = new Report2();
                                report2.setVisible(true);
                                administratorView.setVisible(false);
                                report2.getBtnBack().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        report2.dispose();
                                        administratorView.setVisible(true);
                                    }
                                });
                                report2.getMakeReport1Btn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        deliveryService.rep2(Integer.valueOf(report2.getTxt()));
                                    }
                                });
                            }
                        });

                        ////REPORT 3
                        administratorView.getReport3Btn().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                report3 = new Report3();
                                report3.setVisible(true);
                                administratorView.setVisible(false);
                                report3.getBtnBack().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        report3.dispose();
                                        administratorView.setVisible(true);
                                    }
                                });
                                report3.getMakeReport1Btn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        deliveryService.rep3(Integer.valueOf(report3.getStartTxt()),
                                                Integer.valueOf(report3.getEndTxt()));
                                    }
                                });
                            }
                        });

                        ///////REPORT 4
                        administratorView.getReport4Btn().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                report4 = new Report4();
                                report4.setVisible(true);
                                administratorView.setVisible(false);
                                report4.getBtnBack().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        report4.dispose();
                                        administratorView.setVisible(true);
                                    }
                                });
                                report4.getMakeReport1Btn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        deliveryService.rep4(Integer.valueOf(report4.getTxt()));
                                    }
                                });
                            }
                        });
                    }
                    for(int i = 0; i < model.getUsers().size(); i ++){
                        if(view.getNameTxt().equals(model.getUsers().get(i).getUsername())){
                            if(model.getUsers().get(i).getType() == Role.Client){
                                clientView = new ClientView();
                                clientView.setVisible(true);
                                view.setVisible(false);
                                clientView.getBtnBack().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        clientView.dispose();
                                        view.setVisible(true);
                                    }
                                });
                                /////SHOW
                                clientView.getShowProductsBtn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        productsView = new ProductsView(deliveryService.productsView());
                                        productsView.setVisible(true);
                                        clientView.setVisible(false);
                                        productsView.getBtnBack().addActionListener(new ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent e) {
                                                productsView.dispose();
                                                clientView.setVisible(true);
                                            }
                                        });
                                    }
                                });
                                ///SEARCH
                                clientView.getSearchBtn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        tab = new JTable();
                                        searchView = new SearchView();
                                        searchView.setVisible(true);
                                        clientView.setVisible(false);
                                        searchView.getBtnBack().addActionListener(new ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent e) {
                                                clientView.setVisible(true);
                                                searchView.dispose();
                                            }
                                        });
                                        searchView.getBtnSearch().addActionListener(new ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent e) {
                                                String title = searchView.getTitle();
                                                String rating = searchView.getf1();
                                                String calories = searchView.getf2();
                                                String prot = searchView.getf3();
                                                String fat = searchView.getf4();
                                                String sod = searchView.getf5();
                                                String price = searchView.getf6();
                                                ProductsView productsView2 = new ProductsView(deliveryService.search(title,
                                                        rating, calories, prot, fat, sod, price));
                                                productsView2.setVisible(true);
                                                searchView.setVisible(false);
                                                productsView2.getBtnBack().addActionListener(new ActionListener() {
                                                    @Override
                                                    public void actionPerformed(ActionEvent e) {
                                                        productsView2.dispose();
                                                        searchView.setVisible(true);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                                ////ORDER
                                clientView.getOrderBtn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        orderView = new OrderView();
                                        orderView.setVisible(true);
                                        clientView.setVisible(false);
                                        ArrayList<MenuItem> products = new ArrayList<>();
                                        orderView.getBackBtn().addActionListener(new ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent e) {
                                                orderView.dispose();
                                                clientView.setVisible(true);
                                            }
                                        });
                                        orderView.getPutBtn().addActionListener(new ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent e) {
                                                products.add(deliveryService.findByName(orderView.getComboBox()));
                                            }
                                        });
                                        orderView.getAddBtn().addActionListener(new ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent e) {
                                                deliveryService.addOrder(products, clientID);
                                                System.out.println();
                                                for(MenuItem m : products) {
                                                    m.orderNrplus();
                                                    System.out.println(m.getTitle()+ m.getOrderNr());
                                                }
                                                products.clear();
                                            }
                                        });
                                    }
                                });
                            }
                            if(model.getUsers().get(i).getType() == Role.Employee){
                                employeeView.setVisible(true);
                                view.setVisible(false);
                                employeeView.getBtnBack().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        employeeView.dispose();
                                        view.setVisible(true);
                                    }
                                });
                                employeeView.getSerializareBtn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        Serializator.ser(deliveryService.getOrders(), Model.getUsers(),
                                                deliveryService.getMenu());
                                    }
                                });
                            }
                        }
                    }
                }
                view.clear();
            }
        });
    }
}