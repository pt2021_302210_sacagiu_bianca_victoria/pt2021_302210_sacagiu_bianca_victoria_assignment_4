package PresentationLayer;

import BusinessLayer.DeliveryService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class EditProductView extends JFrame {

    private JPanel contentPane;
    private JTextField txt;
    private JTextField txt1;
    private JTextField txt2;
    private JTextField txt3;
    private JTextField txt4;
    private JTextField txt5;
    private JTextField txt6;
    private JButton btnEdit;
    private JButton btnBack;
    private JComboBox comboBox;

    public EditProductView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 474, 668);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(204, 255, 153));
        contentPane.setForeground(new Color(0, 153, 0));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel adminLbl = new JLabel("Edit product");
        adminLbl.setForeground(new Color(0, 102, 0));
        adminLbl.setFont(new Font("Times New Roman", Font.BOLD, 38));
        adminLbl.setBounds(121, 10, 368, 50);
        contentPane.add(adminLbl);

        btnBack = new JButton("Back");
        btnBack.setForeground(new Color(0, 102, 0));
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 20));
        btnBack.setBackground(new Color(255, 255, 51));
        btnBack.setBounds(10, 572, 85, 35);
        contentPane.add(btnBack);

        JLabel titleLbl = new JLabel("Title:\r\n");
        titleLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        titleLbl.setForeground(new Color(0, 102, 0));
        titleLbl.setBounds(67, 88, 128, 29);
        contentPane.add(titleLbl);

        txt = new JTextField();
        txt.setFont(new Font("Times New Roman", Font.BOLD, 23));
        txt.setBackground(new Color(255, 255, 51));
        txt.setForeground(new Color(0, 102, 0));
        txt.setBounds(190, 88, 204, 29);
        contentPane.add(txt);
        txt.setColumns(10);

        JLabel ratingLbl = new JLabel("Rating:");
        ratingLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        ratingLbl.setForeground(new Color(0, 102, 0));
        ratingLbl.setBackground(new Color(0, 102, 0));
        ratingLbl.setBounds(67, 139, 97, 35);
        contentPane.add(ratingLbl);

        txt1 = new JTextField();
        txt1.setForeground(new Color(0, 102, 0));
        txt1.setFont(new Font("Times New Roman", Font.BOLD, 23));
        txt1.setBackground(new Color(255, 255, 51));
        txt1.setBounds(190, 142, 204, 29);
        contentPane.add(txt1);
        txt1.setColumns(10);

        JLabel calorieslbl = new JLabel("Calories:");
        calorieslbl.setForeground(new Color(0, 102, 0));
        calorieslbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        calorieslbl.setBounds(67, 197, 97, 29);
        contentPane.add(calorieslbl);

        txt2 = new JTextField();
        txt2.setFont(new Font("Times New Roman", Font.BOLD, 23));
        txt2.setForeground(new Color(0, 102, 0));
        txt2.setBackground(new Color(255, 255, 51));
        txt2.setBounds(190, 197, 204, 29);
        contentPane.add(txt2);
        txt2.setColumns(10);

        JLabel proteinLbl = new JLabel("Protein:");
        proteinLbl.setForeground(new Color(0, 102, 0));
        proteinLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        proteinLbl.setBackground(new Color(255, 255, 51));
        proteinLbl.setBounds(67, 250, 117, 29);
        contentPane.add(proteinLbl);

        txt3 = new JTextField();
        txt3.setFont(new Font("Times New Roman", Font.BOLD, 23));
        txt3.setForeground(new Color(0, 102, 0));
        txt3.setBackground(new Color(255, 255, 51));
        txt3.setBounds(190, 250, 204, 29);
        contentPane.add(txt3);
        txt3.setColumns(10);

        JLabel fatLbl = new JLabel("Fat:");
        fatLbl.setForeground(new Color(0, 102, 0));
        fatLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        fatLbl.setBounds(67, 297, 97, 29);
        contentPane.add(fatLbl);

        txt4 = new JTextField();
        txt4.setForeground(new Color(0, 102, 0));
        txt4.setFont(new Font("Times New Roman", Font.BOLD, 23));
        txt4.setBackground(new Color(255, 255, 51));
        txt4.setBounds(190, 297, 204, 29);
        contentPane.add(txt4);
        txt4.setColumns(10);

        JLabel sodiumLbl = new JLabel("Sodium:");
        sodiumLbl.setForeground(new Color(0, 102, 0));
        sodiumLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        sodiumLbl.setBounds(67, 345, 117, 29);
        contentPane.add(sodiumLbl);

        txt5 = new JTextField();
        txt5.setForeground(new Color(0, 102, 0));
        txt5.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 23));
        txt5.setBackground(new Color(255, 255, 51));
        txt5.setBounds(190, 349, 204, 29);
        contentPane.add(txt5);
        txt5.setColumns(10);

        JLabel priceLbl = new JLabel("Price:");
        priceLbl.setForeground(new Color(0, 102, 0));
        priceLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        priceLbl.setBounds(67, 390, 97, 29);
        contentPane.add(priceLbl);

        txt6 = new JTextField();
        txt6.setFont(new Font("Times New Roman", Font.BOLD, 23));
        txt6.setForeground(new Color(0, 102, 0));
        txt6.setBackground(new Color(255, 255, 51));
        txt6.setBounds(190, 394, 204, 29);
        contentPane.add(txt6);
        txt6.setColumns(10);

        btnEdit = new JButton("EDIT");
        btnEdit.setFont(new Font("Times New Roman", Font.BOLD, 20));
        btnEdit.setForeground(new Color(0, 102, 0));
        btnEdit.setBackground(new Color(255, 255, 51));
        btnEdit.setBounds(166, 520, 139, 35);
        contentPane.add(btnEdit);

        comboBox = new JComboBox(DeliveryService.getTitles());
        comboBox.setFont(new Font("Times New Roman", Font.BOLD, 20));
        comboBox.setForeground(new Color(0, 102, 0));
        comboBox.setBackground(new Color(255, 255, 51));
        comboBox.setBounds(67, 455, 327, 29);
        contentPane.add(comboBox);
    }
    public JButton getBtnEdit()
    {
        return this.btnEdit;
    }
    public JButton getBtnBack()
    {
        return this.btnBack;
    }
    public String getField1()
    {
        return this.txt.getText();
    }
    public String getField2()
    {
        return this.txt1.getText();
    }
    public String getField3()
    {
        return this.txt2.getText();
    }
    public String getField4()
    {
        return this.txt3.getText();
    }
    public String getField5()
    {
        return this.txt4.getText();
    }
    public String getField6()
    {
        return this.txt5.getText();

    } public String getField7()
    {
        return this.txt6.getText();
    }
    public void clear(String a)
    {
        this.txt.setText(a);
        this.txt1.setText(a);
        this.txt2.setText(a);
        this.txt3.setText(a);
        this.txt4.setText(a);
        this.txt5.setText(a);
        this.txt6.setText(a);
    }
    public String getCombo()
    {
        return (String) this.comboBox.getSelectedItem();
    }


}
