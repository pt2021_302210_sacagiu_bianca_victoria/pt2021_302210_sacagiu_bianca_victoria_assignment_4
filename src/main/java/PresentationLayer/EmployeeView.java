package PresentationLayer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class EmployeeView extends JFrame implements Observer {
    private JLabel et1;
    private JButton btnBack;
    private JButton serializareBtn;
    private final ArrayList<JLabel> labels= new ArrayList<>();
    private int orderNr = 0;
    private JComboBox comboBox;
    private JPanel contentPane;

    public EmployeeView(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 401, 399);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(255, 204, 102));
        contentPane.setForeground(new Color(0, 153, 0));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel addLbl = new JLabel("EMPLOYEE");
        addLbl.setFont(new Font("Times New Roman", Font.BOLD, 30));
        addLbl.setForeground(new Color(153, 0, 0));
        addLbl.setBounds(95, 10, 348, 56);
        contentPane.add(addLbl);

        serializareBtn = new JButton("Serializare");
        serializareBtn.setBackground(new Color(255, 153, 51));
        serializareBtn.setForeground(new Color(153, 0, 0));
        serializareBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        serializareBtn.setBounds(97, 111, 178, 33);
        contentPane.add(serializareBtn);

        et1 = new JLabel("0");
        et1.setFont(new Font("Times New Roman", Font.BOLD, 26));
        et1.setForeground(new Color(153, 0, 0));
        et1.setBounds(20, 166, 255, 33);
        contentPane.add(et1);


        comboBox = new JComboBox<String>();
        comboBox.setForeground(new Color(102, 0, 0));
        comboBox.setFont(new Font("Times New Roman", Font.BOLD, 22));
        comboBox.setBackground(new Color(255, 153, 51));
        comboBox.setBounds(97, 243, 178, 33);
        contentPane.add(comboBox);

        btnBack = new JButton("Back");
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 20));
        btnBack.setForeground(new Color(102, 0, 0));
        btnBack.setBackground(new Color(255, 153, 51));
        btnBack.setBounds(10, 303, 96, 33);
        contentPane.add(btnBack);

    }

    public JButton getBtnBack() {
        return btnBack;
    }

    public ArrayList<JLabel> getLabels() {
        return labels;
    }

    public void update(Observable o, Object arg) {
        this.orderNr++;
        this.et1.setText(String.valueOf(orderNr));
        comboBox.addItem(arg);
    }

    public JButton getSerializareBtn() {
        return serializareBtn;
    }
}