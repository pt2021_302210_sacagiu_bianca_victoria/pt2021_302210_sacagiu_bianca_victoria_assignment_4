package PresentationLayer;

import BusinessLayer.DeliveryService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class OrderView extends JFrame{
    private ImageIcon icon1;
    private JButton backBtn;
    private JButton addBtn;
    private JButton putBtn;
    private JComboBox comboBox;
    private JPanel contentPane;

    public OrderView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 513, 341);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(153, 204, 204));
        contentPane.setForeground(new Color(0, 153, 0));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        backBtn = new JButton("Back");
        backBtn.setForeground(new Color(102, 102, 153));
        backBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        //backBtn.setBackground(new Color(204, 204, 255));
        backBtn.setBounds(10, 258, 85, 35);
        contentPane.add(backBtn);

        JLabel addLbl = new JLabel("Order Create");
        addLbl.setFont(new Font("Times New Roman", Font.BOLD, 30));
        addLbl.setForeground(new Color(102, 102, 153));
        addLbl.setBounds(141, 22, 348, 56);
        contentPane.add(addLbl);

        comboBox = new JComboBox(DeliveryService.getTitles());
        comboBox.setFont(new Font("Times New Roman", Font.BOLD, 20));
        comboBox.setForeground(new Color(102, 102, 153));
        comboBox.setBackground(new Color(204, 204, 255));
        comboBox.setBounds(36, 106, 245, 29);
        contentPane.add(comboBox);

        putBtn = new JButton("Put");
        //putBtn.setBackground(new Color(204, 204, 255));
        putBtn.setForeground(new Color(102, 102, 153));
        putBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        putBtn.setBounds(336, 106, 105, 29);
        contentPane.add(putBtn);

        addBtn = new JButton("Create order");
        addBtn.setForeground(new Color(102, 102, 153));
        addBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
       // addBtn.setBackground(new Color(204, 204, 255));
        addBtn.setBounds(143, 183, 182, 35);
        contentPane.add(addBtn);
    }

    public String getComboBox() {
        return (String) comboBox.getSelectedItem();
    }

    public JButton getAddBtn() {
        return addBtn;
    }

    public JButton getBackBtn() {
        return backBtn;
    }

    public JButton getPutBtn() {
        return putBtn;
    }
}
