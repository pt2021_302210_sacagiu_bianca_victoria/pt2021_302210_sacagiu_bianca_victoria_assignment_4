package PresentationLayer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class AdministratorView extends JFrame{
    JButton btnBack;
    JButton importBtn;
    JButton addBtn;
    JButton deleteBtn;
    JButton editBtn;
    JButton report1Btn;
    JButton report2Btn;
    JButton report3Btn;
    JButton report4Btn;
    JButton addComposedBtn;
    private JPanel contentPane;

    public AdministratorView(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 670, 427);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(204, 255, 153));
        contentPane.setForeground(new Color(0, 153, 0));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel adminLbl = new JLabel("ADMINISTRATOR");
        adminLbl.setForeground(new Color(0, 102, 0));
        adminLbl.setFont(new Font("Times New Roman", Font.BOLD, 38));
        adminLbl.setBounds(152, 30, 368, 50);
        contentPane.add(adminLbl);

        importBtn = new JButton("Import Data\r\n");
        importBtn.setBackground(new Color(255, 255, 51));
        importBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        importBtn.setForeground(new Color(0, 102, 0));
        importBtn.setBounds(34, 127, 149, 35);
        contentPane.add(importBtn);

        addBtn = new JButton("Add Product");
        addBtn.setBackground(new Color(255, 255, 51));
        addBtn.setForeground(new Color(0, 102, 0));
        addBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        addBtn.setBounds(215, 127, 159, 35);
        contentPane.add(addBtn);

        addComposedBtn = new JButton("Add composite product");
        addComposedBtn.setBackground(new Color(255, 255, 51));
        addComposedBtn.setForeground(new Color(0, 102, 0));
        addComposedBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        addComposedBtn.setBounds(401, 127, 238, 35);
        contentPane.add(addComposedBtn);

        editBtn = new JButton("Edit Product");
        editBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        editBtn.setForeground(new Color(0, 102, 0));
        editBtn.setBackground(new Color(255, 255, 51));
        editBtn.setBounds(34, 197, 149, 35);
        contentPane.add(editBtn);

        deleteBtn = new JButton("Delete Product\r\n");
        deleteBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        deleteBtn.setForeground(new Color(0, 102, 0));
        deleteBtn.setBackground(new Color(255, 255, 51));
        deleteBtn.setBounds(215, 197, 162, 35);
        contentPane.add(deleteBtn);

        report1Btn = new JButton("Report 1");
        report1Btn.setForeground(new Color(0, 102, 0));
        report1Btn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        report1Btn.setBackground(new Color(255, 255, 51));
        report1Btn.setBounds(34, 263, 149, 35);
        contentPane.add(report1Btn);

        report2Btn = new JButton("Report 2");
        report2Btn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        report2Btn.setForeground(new Color(0, 102, 0));
        report2Btn.setBackground(new Color(255, 255, 51));
        report2Btn.setBounds(215, 263, 159, 35);
        contentPane.add(report2Btn);

        report3Btn = new JButton("Report 3\r\n");
        report3Btn.setForeground(new Color(0, 102, 0));
        report3Btn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        report3Btn.setBackground(new Color(255, 255, 51));
        report3Btn.setBounds(401, 263, 139, 35);
        contentPane.add(report3Btn);

        report4Btn = new JButton("Report 4");
        report4Btn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        report4Btn.setForeground(new Color(0, 102, 0));
        report4Btn.setBackground(new Color(255, 255, 51));
        report4Btn.setBounds(401, 197, 139, 35);
        contentPane.add(report4Btn);

        btnBack = new JButton("Back");
        btnBack.setForeground(new Color(0, 102, 0));
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 20));
        btnBack.setBackground(new Color(255, 255, 51));
        btnBack.setBounds(10, 333, 85, 35);
        contentPane.add(btnBack);
    }

    public JButton getBtnBack() {
        return btnBack;
    }

    public JButton getImportBtn() {
        return importBtn;
    }

    public JButton getReport1Btn() {
        return report1Btn;
    }

    public JButton getReport2Btn() {
        return report2Btn;
    }

    public JButton getReport3Btn() {
        return report3Btn;
    }

    public JButton getReport4Btn() {
        return report4Btn;
    }
}
