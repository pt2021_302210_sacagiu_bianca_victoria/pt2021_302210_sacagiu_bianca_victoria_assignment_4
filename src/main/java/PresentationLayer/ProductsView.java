package PresentationLayer;

import BusinessLayer.DeliveryService;
import BusinessLayer.MenuItem;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.HashSet;


public class ProductsView extends JFrame {

    private JButton btnBack;
    private JScrollPane scrollPane;
    private JTable table;

    public ProductsView(HashSet<MenuItem> menu) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 500, 500);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        btnBack = new JButton("Back");
        btnBack.setForeground(new Color(102, 102, 153));
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 20));
        btnBack.setBounds(50, 415, 100, 40);
        contentPane.add(btnBack);

        String[] cols = {"Title", "Rating", "Calories", "Protein",
                "Fat", "Sodium", "Price"};
        Object[][] obj = new Object[menu.size()][cols.length];

        int j = 0;
        for (MenuItem i : menu) {
            obj[j][0] = i.getTitle();
            obj[j][1] = i.getRating();
            obj[j][2] = i.getCalories();
            obj[j][3] = i.getProtein();
            obj[j][4] = i.getFat();
            obj[j][5] = i.getSodium();
            obj[j][6] = i.getPrice();

            j++;
        }
        table = new JTable(obj, cols);
        scrollPane = new JScrollPane();
        scrollPane.setBounds(0, 0, 500, 500);
        contentPane.add(scrollPane);
        contentPane.setLayout(null);
        table.setBackground(new Color(153, 204, 204));
        table.setForeground(new Color(0, 51, 102));
        scrollPane.setViewportView(table);


    }

    public JButton getBtnBack() {
        return btnBack;
    }
}
