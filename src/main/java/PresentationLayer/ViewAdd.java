package PresentationLayer;

import BusinessLayer.Role;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class ViewAdd extends JFrame {
    private JPanel contentPane;
    private  JTextField  nameTxt;
    private  JPasswordField  passTxt;
    private JButton btnBack;
    private  JButton btnIn;
    private JComboBox comboBox;
    private Role[]  roles = {Role.Client,Role.Employee};

    public ViewAdd() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 399, 414);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(221, 160, 221));
        contentPane.setForeground(new Color(221, 160, 221));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel signUpLbl = new JLabel("Sign Up");
        signUpLbl.setForeground(new Color(75, 0, 130));
        signUpLbl.setFont(new Font("Times New Roman", Font.BOLD, 37));
        signUpLbl.setBackground(new Color(123, 104, 238));
        signUpLbl.setBounds(117, 25, 208, 41);
        contentPane.add(signUpLbl);

        JLabel usernameLbl = new JLabel("Username: ");
        usernameLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        usernameLbl.setForeground(new Color(75, 0, 130));
        usernameLbl.setBounds(32, 87, 145, 41);
        contentPane.add(usernameLbl);

        JLabel passLbl = new JLabel("Password:");
        passLbl.setForeground(new Color(75, 0, 130));
        passLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        passLbl.setBounds(32, 138, 129, 41);
        contentPane.add(passLbl);

        btnIn = new JButton("SignUp");
        btnIn.setFont(new Font("Times New Roman", Font.BOLD, 14));
        btnIn.setForeground(new Color(75, 0, 130));
        btnIn.setBackground(new Color(148, 0, 211));
        btnIn.setBounds(131, 257, 113, 34);
        contentPane.add(btnIn);

        nameTxt = new JTextField();
        nameTxt.setFont(new Font("Times New Roman", Font.BOLD, 14));
        nameTxt.setBackground(new Color(230, 230, 250));
        nameTxt.setBounds(155, 96, 156, 29);
        contentPane.add(nameTxt);
        nameTxt.setColumns(10);

        passTxt = new JPasswordField();
        passTxt.setBackground(new Color(230, 230, 250));
        passTxt.setFont(new Font("Times New Roman", Font.BOLD, 14));
        passTxt.setBounds(155, 147, 156, 29);
        contentPane.add(passTxt);
        passTxt.setColumns(10);

        btnBack = new JButton("Back");
        btnBack.setForeground(new Color(75, 0, 130));
        btnBack.setFont(new Font("Tahoma", Font.BOLD, 12));
        btnBack.setBackground(new Color(148, 0, 211));
        btnBack.setBounds(30, 317, 90, 29);
        contentPane.add(btnBack);

        JLabel roleLbl = new JLabel("Role:");
        roleLbl.setForeground(new Color(0, 0, 128));
        roleLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        roleLbl.setBounds(32, 193, 106, 29);
        contentPane.add(roleLbl);

        comboBox = new JComboBox(roles);
        comboBox.setBackground(new Color(230, 230, 250));
        comboBox.setBounds(155, 197, 156, 29);
        contentPane.add(comboBox);

    }

    public JButton getBtnBack() {
        return btnBack;
    }

    public Role getComboBox() {
        return (Role) comboBox.getSelectedItem();
    }

    public String getNameTxt() {
        return nameTxt.getText();
    }

    public String getPassTxt(){
        return passTxt.getText();
    }

    public JButton getBtnIn() {
        return btnIn;
    }

    public void clear(){
        passTxt.setText("");
        nameTxt.setText("");
    }
}