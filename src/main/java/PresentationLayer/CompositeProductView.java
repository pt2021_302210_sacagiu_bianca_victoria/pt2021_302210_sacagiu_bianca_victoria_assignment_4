package PresentationLayer;


import BusinessLayer.DeliveryService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class CompositeProductView extends JFrame {

    private JPanel contentPane;
    private JTextField textField;
    private JButton backBtn;
    private JButton addBtn;
    private JButton putBtn;
    private JComboBox comboBox;

    public CompositeProductView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 513, 341);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(204, 255, 153));
        contentPane.setForeground(new Color(0, 153, 0));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        backBtn = new JButton("Back");
        backBtn.setForeground(new Color(0, 102, 0));
        backBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        backBtn.setBackground(new Color(255, 255, 51));
        backBtn.setBounds(10, 258, 85, 35);
        contentPane.add(backBtn);

        JLabel addLbl = new JLabel("Add composite product");
        addLbl.setFont(new Font("Times New Roman", Font.BOLD, 30));
        addLbl.setForeground(new Color(0, 102, 0));
        addLbl.setBounds(93, 23, 348, 56);
        contentPane.add(addLbl);

        comboBox = new JComboBox(DeliveryService.getTitles());
        comboBox.setFont(new Font("Times New Roman", Font.BOLD, 20));
        comboBox.setForeground(new Color(0, 102, 0));
        comboBox.setBackground(new Color(255, 255, 51));
        comboBox.setBounds(36, 106, 245, 29);
        contentPane.add(comboBox);

        putBtn = new JButton("Put");
        putBtn.setBackground(new Color(255, 255, 51));
        putBtn.setForeground(new Color(0, 102, 0));
        putBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        putBtn.setBounds(336, 106, 105, 29);
        contentPane.add(putBtn);

        textField = new JTextField();
        textField.setForeground(new Color(0, 102, 0));
        textField.setFont(new Font("Times New Roman", Font.BOLD, 20));
        textField.setBackground(new Color(255, 255, 51));
        textField.setBounds(36, 167, 245, 29);
        contentPane.add(textField);
        textField.setColumns(10);

        addBtn = new JButton("Add");
        addBtn.setForeground(new Color(0, 102, 0));
        addBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        addBtn.setBackground(new Color(255, 255, 51));
        addBtn.setBounds(336, 170, 105, 29);
        contentPane.add(addBtn);
    }
    public JButton getBackBtn()
    {
        return this.backBtn;
    }
    public  JButton getAddBtn()
    {
        return this.addBtn;
    }
    public JButton getPutBtn()
    {
        return this.putBtn;
    }
    public String getTitle()
    {
        return this.textField.getText();
    }
    public void setField(String a)
    {
        this.textField.setText(a);
    }
    public String getCombo()
    {
        return (String) this.comboBox.getSelectedItem();
    }

}
