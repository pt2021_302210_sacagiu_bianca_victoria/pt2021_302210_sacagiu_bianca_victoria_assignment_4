package PresentationLayer;

import BusinessLayer.DeliveryService;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;


public class SearchView extends JFrame {

    private JButton btnSearch;
    private JButton btnBack1;
    private JTextField f1;
    private JTextField f2;
    private JTextField f3;
    private JTextField f4;
    private JTextField f5;
    private JTextField f6;
    private JTextField f7;
    private JPanel contentPane;

    public SearchView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 474, 578);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(153, 204, 204));
        contentPane.setForeground(new Color(0, 153, 0));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel adminLbl = new JLabel("Search product");
        adminLbl.setForeground(new Color(102, 102, 153));
        adminLbl.setFont(new Font("Times New Roman", Font.BOLD, 38));
        adminLbl.setBounds(104, 10, 368, 50);
        contentPane.add(adminLbl);

        btnBack1 = new JButton("Back");
        btnBack1.setForeground(new Color(102, 102, 153));
        btnBack1.setFont(new Font("Times New Roman", Font.BOLD, 20));
       // btnBack1.setBackground(new Color(255, 255, 51));
        btnBack1.setBounds(10, 485, 85, 35);
        contentPane.add(btnBack1);

        JLabel titleLbl = new JLabel("Title:\r\n");
        titleLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        titleLbl.setForeground(new Color(102, 102, 153));
        titleLbl.setBounds(67, 88, 128, 29);
        contentPane.add(titleLbl);

        f1 = new JTextField();
        f1.setFont(new Font("Times New Roman", Font.BOLD, 23));
        f1.setBackground(new Color(204, 204, 255));
        f1.setForeground(new Color(0, 102, 0));
        f1.setBounds(190, 88, 204, 29);
        contentPane.add(f1);
        f1.setColumns(10);

        JLabel ratingLbl = new JLabel("Rating:");
        ratingLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        ratingLbl.setForeground(new Color(102, 102, 153));
        ratingLbl.setBackground(new Color(0, 102, 0));
        ratingLbl.setBounds(67, 139, 97, 35);
        contentPane.add(ratingLbl);

        f2 = new JTextField();
        f2.setForeground(new Color(0, 102, 0));
        f2.setFont(new Font("Times New Roman", Font.BOLD, 23));
        f2.setBackground(new Color(204, 204, 255));
        f2.setBounds(190, 142, 204, 29);
        contentPane.add(f2);
        f2.setColumns(10);

        JLabel calorieslbl = new JLabel("Calories:");
        calorieslbl.setForeground(new Color(102, 102, 153));
        calorieslbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        calorieslbl.setBounds(67, 197, 97, 29);
        contentPane.add(calorieslbl);

        f3 = new JTextField();
        f3.setFont(new Font("Times New Roman", Font.BOLD, 23));
        f3.setForeground(new Color(0, 102, 0));
        f3.setBackground(new Color(204, 204, 255));
        f3.setBounds(190, 197, 204, 29);
        contentPane.add(f3);
        f3.setColumns(10);

        JLabel proteinLbl = new JLabel("Protein:");
        proteinLbl.setForeground(new Color(102, 102, 153));
        proteinLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        proteinLbl.setBackground(new Color(255, 255, 51));
        proteinLbl.setBounds(67, 250, 117, 29);
        contentPane.add(proteinLbl);

        f4 = new JTextField();
        f4.setFont(new Font("Times New Roman", Font.BOLD, 23));
        f4.setForeground(new Color(0, 102, 0));
        f4.setBackground(new Color(204, 204, 255));
        f4.setBounds(190, 250, 204, 29);
        contentPane.add(f4);
        f4.setColumns(10);

        JLabel fatLbl = new JLabel("Fat:");
        fatLbl.setForeground(new Color(102, 102, 153));
        fatLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        fatLbl.setBounds(67, 297, 97, 29);
        contentPane.add(fatLbl);

        f5 = new JTextField();
        f5.setForeground(new Color(0, 102, 0));
        f5.setFont(new Font("Times New Roman", Font.BOLD, 23));
        f5.setBackground(new Color(204, 204, 255));
        f5.setBounds(190, 297, 204, 29);
        contentPane.add(f5);
        f5.setColumns(10);

        JLabel sodiumLbl = new JLabel("Sodium:");
        sodiumLbl.setForeground(new Color(102, 102, 153));
        sodiumLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        sodiumLbl.setBounds(67, 345, 117, 29);
        contentPane.add(sodiumLbl);

        f6 = new JTextField();
        f6.setForeground(new Color(0, 102, 0));
        f6.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 23));
        f6.setBackground(new Color(204, 204, 255));
        f6.setBounds(190, 349, 204, 29);
        contentPane.add(f6);
        f6.setColumns(10);

        JLabel priceLbl = new JLabel("Price:");
        priceLbl.setForeground(new Color(102, 102, 153));
        priceLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        priceLbl.setBounds(67, 390, 97, 29);
        contentPane.add(priceLbl);

        f7 = new JTextField();
        f7.setFont(new Font("Times New Roman", Font.BOLD, 23));
        f7.setForeground(new Color(0, 102, 0));
        f7.setBackground(new Color(204, 204, 255));
        f7.setBounds(190, 394, 204, 29);
        contentPane.add(f7);
        f7.setColumns(10);

        btnSearch = new JButton("SEARCH");
        btnSearch.setFont(new Font("Times New Roman", Font.BOLD, 20));
        btnSearch.setForeground(new Color(102, 102, 153));
       // btnSearch.setBackground(new Color(255, 255, 51));
        btnSearch.setBounds(172, 459, 139, 35);
        contentPane.add(btnSearch);

    }

    public JButton getBtnBack() {
        return btnBack1;
    }


    public String getf1() {
        return f1.getText();
    }

    public String getf2() {
        return f2.getText();
    }

    public String getf3() {
        return f3.getText();
    }

    public String getf4() {
        return f4.getText();
    }

    public String getf5() {
        return f5.getText();
    }

    public String getf6() {
        return f6.getText();
    }

    public String getf7() {
        return f7.getText();
    }

    public JButton getBtnSearch() {
        return btnSearch;
    }

}
