package PresentationLayer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class View extends JFrame {

    private JTextField field1;
    private JPasswordField field2;
    private JLabel et1;
    private JLabel et2;
    private JPanel contentPane;
    private JLabel et3;
    private JLabel et4;
    private JButton btnNew;
    private JButton loginButton;
    private ImageIcon icon1;
    private ImageIcon icon3;
    private ImageIcon icon2;
    private ImageIcon icon;
    private ImageIcon icon4;

    public View() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 399, 414);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(221, 160, 221));
        contentPane.setForeground(new Color(221, 160, 221));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel signUpLbl = new JLabel("Sign In");
        signUpLbl.setForeground(new Color(75, 0, 130));
        signUpLbl.setFont(new Font("Times New Roman", Font.BOLD, 37));
        signUpLbl.setBackground(new Color(123, 104, 238));
        signUpLbl.setBounds(117, 25, 208, 41);
        contentPane.add(signUpLbl);

        JLabel usernameLbl = new JLabel("Username: ");
        usernameLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        usernameLbl.setForeground(new Color(75, 0, 130));
        usernameLbl.setBounds(32, 116, 145, 41);
        contentPane.add(usernameLbl);

        JLabel passLbl = new JLabel("Password:");
        passLbl.setForeground(new Color(75, 0, 130));
        passLbl.setFont(new Font("Times New Roman", Font.BOLD, 23));
        passLbl.setBounds(32, 181, 129, 41);
        contentPane.add(passLbl);

        loginButton = new JButton("Login");
        loginButton.setFont(new Font("Times New Roman", Font.BOLD, 14));
        loginButton.setForeground(new Color(75, 0, 130));
        loginButton.setBackground(new Color(148, 0, 211));
        loginButton.setBounds(131, 257, 113, 34);
        contentPane.add(loginButton);

        field1 = new JTextField();
        field1.setFont(new Font("Times New Roman", Font.BOLD, 14));
        field1.setBackground(new Color(230, 230, 250));
        field1.setBounds(155, 126, 156, 29);
        contentPane.add(field1);
        field1.setColumns(10);

        field2 = new JPasswordField();
        field2.setBackground(new Color(230, 230, 250));
        field2.setFont(new Font("Times New Roman", Font.BOLD, 14));
        field2.setBounds(155, 191, 156, 29);
        contentPane.add(field2);
        field2.setColumns(10);

        btnNew = new JButton("Sign Up");
        btnNew.setForeground(new Color(75, 0, 130));
        btnNew.setFont(new Font("Tahoma", Font.BOLD, 12));
        btnNew.setBackground(new Color(148, 0, 211));
        btnNew.setBounds(30, 317, 90, 29);
        contentPane.add(btnNew);
    }

    public JButton getLoginButton()
    {
        return this.loginButton;
    }
    public String getUsername()
    {
        return this.field1.getText();
    }
    public String getPassword()
    {
        return this.field2.getText();
    }

    public JButton getBtnNew() {
        return btnNew;
    }

    public String getPass() {
        return field2.getText();
    }

    public String getNameTxt() {
        return field1.getText();
    }

    public void clear(){
        field1.setText("");
        field2.setText("");
    }
}