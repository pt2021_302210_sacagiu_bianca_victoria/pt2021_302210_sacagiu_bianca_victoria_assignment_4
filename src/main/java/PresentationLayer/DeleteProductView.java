package PresentationLayer;

import BusinessLayer.DeliveryService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class DeleteProductView extends JFrame {

    private JPanel contentPane;
    private JButton backBtn;
    private JButton deleteBtn;
    private JComboBox comboBox;
    public DeleteProductView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 474, 308);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(204, 255, 153));
        contentPane.setForeground(new Color(0, 153, 0));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel adminLbl = new JLabel("Delete product");
        adminLbl.setForeground(new Color(0, 102, 0));
        adminLbl.setFont(new Font("Times New Roman", Font.BOLD, 38));
        adminLbl.setBounds(121, 10, 368, 50);
        contentPane.add(adminLbl);

        backBtn = new JButton("Back");
        backBtn.setForeground(new Color(0, 102, 0));
        backBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        backBtn.setBackground(new Color(255, 255, 51));
        backBtn.setBounds(20, 220, 85, 35);
        contentPane.add(backBtn);

        deleteBtn = new JButton("DELETE");
        deleteBtn.setFont(new Font("Times New Roman", Font.BOLD, 20));
        deleteBtn.setForeground(new Color(0, 102, 0));
        deleteBtn.setBackground(new Color(255, 255, 51));
        deleteBtn.setBounds(145, 173, 139, 35);
        contentPane.add(deleteBtn);

        comboBox = new JComboBox(DeliveryService.getTitles());
        comboBox.setFont(new Font("Times New Roman", Font.BOLD, 20));
        comboBox.setForeground(new Color(0, 102, 0));
        comboBox.setBackground(new Color(255, 255, 51));
        comboBox.setBounds(63, 106, 327, 29);
        contentPane.add(comboBox);
    }
    public JButton getBackBtn()
    {
        return this.backBtn;
    }
    public JButton getDeleteBtn()
    {
        return this.deleteBtn;
    }
    public String getCombo()
    {
        return (String) this.comboBox.getSelectedItem();
    }
}
