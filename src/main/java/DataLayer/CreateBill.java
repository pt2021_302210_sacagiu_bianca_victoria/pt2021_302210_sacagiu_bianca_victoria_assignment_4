package DataLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.User;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;

public class CreateBill {

    public void report1(ArrayList<Order> ord){
        String file = "Report1.txt";
        new File(file);
        FileWriter rep1 = null;
        try {
            rep1 = new FileWriter(file);
            rep1.write("Report 1\n\n");
            for(Order o: ord){
                rep1.write("Order ID: " + o.getID() + "\n");
                rep1.write("Client ID: " + o.getClientID() + "\n\n");
                rep1.write("Price: " + o.getPrice());
                rep1.write("\n\nProducts: ");
                ArrayList<MenuItem> menuItems = o.getOrder();
                for(MenuItem m: menuItems)
                {
                    rep1.write(m.getTitle()+"\n");
                }
                rep1.write("\n\n");
            }
            rep1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void report2(ArrayList<MenuItem> ord){
        String file = "Report2.txt";
        new File(file);
        FileWriter rep1 = null;
        try {
            rep1 = new FileWriter(file);
            rep1.write("Report 2\n\n");
            rep1.write("Products: \n");
            for(MenuItem m: ord){
                rep1.write(m.getTitle() + " = " + m.getOrderNr() + " times. \n");
            }
            rep1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void report3(ArrayList<User> u){
        String file = "Report3.txt";
        new File(file);
        FileWriter rep1 = null;
        try {
            rep1 = new FileWriter(file);
            rep1.write("Report 3\n");
            for(User user: u){
                rep1.write("\nUsername: " + user.getUsername() + "\nID: " + user.getID());
            }
            rep1.write("\n\n");
            rep1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void report4(ArrayList<MenuItem> ord){
        String file = "Report4.txt";
        LinkedHashSet<MenuItem> hashSet = new LinkedHashSet<>(ord);
        ArrayList<MenuItem> listWithoutDuplicates = new ArrayList<>(hashSet);
        new File(file);
        FileWriter rep1 = null;
        try {
            rep1 = new FileWriter(file);
            rep1.write("Report 4\n\n");
            rep1.write("Products: \n");
            for(MenuItem m: listWithoutDuplicates){
                rep1.write(m.getTitle() + " = " + m.getOrderNr() + " times. \n");
            }
            rep1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
