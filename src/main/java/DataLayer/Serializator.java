package DataLayer;

import BusinessLayer.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;

public class Serializator {
    static String file1 = "D:\\An 2\\Sem2\\TP\\tema4\\src\\main\\resources\\file1.txt";
    static String file2 = "D:\\An 2\\Sem2\\TP\\tema4\\src\\main\\resources\\file2.txt";
    static String file3 = "D:\\An 2\\Sem2\\TP\\tema4\\src\\main\\resources\\file3.txt";
    static String file4 = "D:\\An 2\\Sem2\\TP\\tema4\\src\\main\\resources\\file4.txt";

    public static void ser(HashSet<Order> orders, ArrayList<User> users, HashSet<MenuItem> menuItems) {
        PrintWriter ser = null;
        try {
            ser = new PrintWriter(file1);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ser.print("");
        ser.close();
        try {
            ser = new PrintWriter(file2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ser.print("");
        ser.close();
        try {
            ser = new PrintWriter(file3);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ser.print("");
        ser.close();
        try {
            FileOutputStream out = new FileOutputStream(file1);
            ObjectOutputStream out1 = new ObjectOutputStream(out);
            out1.writeObject(orders);
            out1.close();
            out.close();
            out = new FileOutputStream(file2);
            out1 = new ObjectOutputStream(out);
            out1.writeObject(menuItems);
            out1.close();
            out.close();
            out = new FileOutputStream(file3);
            out1 = new ObjectOutputStream(out);
            out1.writeObject(users);
            out1.close();
            out.close();
            out = new FileOutputStream(file4);
            out1 = new ObjectOutputStream(out);
            out1.writeObject(User.getK());
            out1.writeObject(Order.getContor());
            out1.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deser(){
        int var;
        int var2;
        ArrayList<User> users1;
        HashSet<Order> orders1;
        HashSet<MenuItem> menuItems1;
        try
        {
            FileInputStream out = new FileInputStream(file1);
            ObjectInputStream out1 = new ObjectInputStream(out);
            orders1 = (HashSet<Order>) out1.readObject();
            out1.close();
            out.close();
            out = new FileInputStream(file2);
            out1 = new ObjectInputStream(out);
            menuItems1 = (HashSet<MenuItem>) out1.readObject();
            out1.close();
            out.close();
            out = new FileInputStream(file3);
            out1 = new ObjectInputStream(out);
            users1 = (ArrayList<User>) out1.readObject();
            out1.close();
            out.close();
            out = new FileInputStream(file4);
            out1 = new ObjectInputStream(out);
            var = (int) out1.readObject();
            var2 = (int) out1.readObject();
            out1.close();
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return;
        }
        catch (ClassNotFoundException e1)
        {
            System.out.println("Class not found");
            e1.printStackTrace();
            return;
        }
        User.setK(var);
        Order.setContor(var2);
        DeliveryService.setMenu(menuItems1);
        DeliveryService.setOrders(orders1);
        Model.setUsers(users1);
    }
}
